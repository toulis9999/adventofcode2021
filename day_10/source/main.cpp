#include "common.h"

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <stack>
#include <string>

int main()
{
	auto nav_lines = parse_multiline_file<std::string>("input.txt");

	auto get_closing = [](char smb) {
		switch (smb)
		{
		case ')':
			return '(';
		case ']':
			return '[';
		case '}':
			return '{';
		case '>':
			return '<';
		default:
			return '\0';
		}
	};

	// problem 1
	{
		auto get_cost = [](char smb) {
			switch (smb)
			{
			case ')':
				return 3;
			case ']':
				return 57;
			case '}':
				return 1197;
			case '>':
				return 25137;
			default:
				return 0;
			}
		};

		auto sum_of_illegals = 0;
		for (const auto& nav_line : nav_lines)
		{
			std::stack<char> scratch;
			for (const auto& smb : nav_line)
			{
				if (smb == '(' || smb == '[' || smb == '{' || smb == '<')
					scratch.push(smb);
				else
				{
					if (scratch.empty())  // input never starts with a closing so this is redundant in this case
					{
						sum_of_illegals += get_cost(smb);
						break;
					}
					auto last_symbol = scratch.top();
					scratch.pop();
					if (get_closing(smb) != last_symbol)
					{
						sum_of_illegals += get_cost(smb);
						break;
					}
				}
			}
		}
		std::cout << sum_of_illegals << "\n";
	}

	// problem 2
	{
		auto get_cost = [](char smb) {
			switch (smb)
			{
			case '(':
				return 1;
			case '[':
				return 2;
			case '{':
				return 3;
			case '<':
				return 4;
			default:
				return 0;
			}
		};

		std::vector<uint64_t> scores;
		for (const auto& nav_line : nav_lines)
		{
			std::vector<char> scratch;
			for (const auto& smb : nav_line)
			{
				if (smb == '(' || smb == '[' || smb == '{' || smb == '<')
					scratch.push_back(smb);
				else
				{
					if (scratch.empty())
						break;
					auto last_symbol = scratch.back();
					scratch.pop_back();
					if (get_closing(smb) != last_symbol)
					{
						scratch.clear();
						break;
					}
				}
			}
			if (!scratch.empty())
				scores.push_back(std::accumulate(
				    scratch.crbegin(), scratch.crend(), static_cast<uint64_t>(0), [&get_cost](uint64_t a, char b) {
					    return (a * 5) + get_cost(b);
				    }));
		}
		std::nth_element(scores.begin(), scores.begin() + scores.size() / 2, scores.end());
		const auto mid_point = *(scores.begin() + scores.size() / 2);
		std::cout << mid_point << "\n";
	}

	return 0;
}
