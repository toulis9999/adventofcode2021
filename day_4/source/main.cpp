#include <array>
#include <bitset>
#include <cstddef>
#include <iostream>
#include <numeric>

#include "common.h"

constexpr size_t board_size = 5;  // from definition

struct Board
{
	// returns non-zero on bingo
	size_t match_and_mark_num(int n)
	{
		for (size_t i = 0; i < board_size; ++i)
			for (size_t j = 0; j < board_size; ++j)
				if (m_data[i][j] == n && !m_found[i][j])
				{
					m_found[i][j] = true;
					m_score -= n;
					if (has_bingo())
					{
						m_has_bingo = true;
						return m_score * n;
					}
					return 0;
				}
		return 0;
	}

	bool has_bingo() const
	{
		// check horizonal
		for (size_t i = 0; i < board_size; ++i)
			if (m_found[i].all())
				return true;
		// check vertical
		for (size_t i = 0; i < board_size; ++i)
		{
			std::bitset<board_size> line;
			for (size_t j = 0; j < board_size; ++j)
			{
				line[j] = m_found[j][i];
				if (line.all())
					return true;
			}
		}
		return false;
	}

	std::array<std::array<int, board_size>, board_size> m_data{};
	std::array<std::bitset<board_size>, board_size> m_found{};

	int m_score{0};
	bool m_has_bingo{false};
};

int main()
{
	ParserHelper p("input.txt");
	auto nums = p.read_delimited<int>(',');
	p.skip_line();

	std::vector<Board> boards;
	while (p.is_good())
	{
		boards.push_back({});
		for (size_t i = 0; i < board_size; ++i)
		{
			p.read_delimited_n_into<int, board_size>(boards.back().m_data[i].data());
			boards.back().m_score =
			    std::accumulate(boards.back().m_data[i].cbegin(), boards.back().m_data[i].cend(), boards.back().m_score);
		}
		p.skip_line();
	}

	auto get_score = [&nums, &boards]() {
		for (const auto& num : nums)
			for (auto& board : boards)
				if (!board.m_has_bingo)
				{
					const auto score = board.match_and_mark_num(num);
					if (score)
						return score;
				}
		return static_cast<size_t>(0);
	};

	// problem 1
	{
		std::cout << get_score() << "\n";
	}

	// problem 2
	{
		size_t prev_score = 0;
		do
		{
			auto score = get_score();
			if (!score)
				break;
			prev_score = score;
		} while (prev_score);
		std::cout << prev_score << "\n";
	}

	return 0;
}
