#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <array>

#include "common.h"

//left in for educational purposes
// all positive integer solutions for (x * 7) + (y * 9) <= 80 - init;
//int attempt_1(int init, int total)
//{
//	std::vector<int> perm;
//	auto left = total - init;
//	auto times_9_can_fit = left / 9;
//	auto sum_of_perms = 0;
//	for (int i = 0; i <= times_9_can_fit; ++i)
//	{
//		auto new_left = left - (9 * i);
//		auto div_7 = new_left / 7;
//		// auto mm = new_left % 7;
//		perm.insert(perm.end(), div_7, 7);
//		perm.insert(perm.end(), i, 9);
//		auto num_perms = 0;
//		do
//		{
//			// verify
//			// auto vvv = std::accumulate(perm.begin(), perm.end(), init);
//			// assert(vvv <= total);
//			// assert(total - vvv < 7);
//			++num_perms;
//		} while (std::next_permutation(perm.begin(), perm.end()));
//		sum_of_perms += num_perms * perm.size();
//		perm.clear();
//	}
//	return sum_of_perms + 1;
//}

int main()
{
	ParserHelper p("input.txt");
	auto fish = p.read_delimited<int>(',');

	std::array<size_t, 10> m{};
	for (const auto& f : fish)
	{
		m[f] += 1;
	}

	auto process = [](std::array<size_t, 10> initial_state, int days) {
		while (days)
		{
			std::array<size_t, 10> next_step{};
			next_step[7] = initial_state[8];
			next_step[6] = initial_state[7];
			next_step[5] = initial_state[6];
			next_step[4] = initial_state[5];
			next_step[3] = initial_state[4];
			next_step[2] = initial_state[3];
			next_step[1] = initial_state[2];
			next_step[0] = initial_state[1];
			// make 0s 6s and generate 8s
			next_step[6] += initial_state[0];
			next_step[8] = initial_state[0];
			initial_state = next_step;
			days--;
		}
		return std::accumulate(initial_state.begin(), initial_state.end(), static_cast<size_t>(0));
	};
	// problem 1
	{
		std::cout << process(m, 80) << "\n";
	}
	// problem 2
	{
		std::cout << process(m, 256) << "\n";
	}

	return 0;
}
