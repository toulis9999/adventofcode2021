#pragma once

#include <fstream>
#include <vector>

template<typename Fn>
std::vector<typename std::result_of<Fn(std::ifstream&)>::type> parse_multiline_file(const char* path, Fn&& f)
{
	using T = typename std::result_of<Fn(std::ifstream&)>::type;
	std::vector<T> ret;
	auto inputfile = std::ifstream(path, std::ios::in);
	while (inputfile.good())
	{
		auto tok = f(inputfile);
		ret.push_back(std::move(tok));
	}
	return ret;
}

template<typename T>
std::vector<T> parse_multiline_file(const char* path)
{
	return parse_multiline_file(path, [](std::ifstream& s) {
		T tok;
		s >> tok;
		return tok;
	});
}

// practically zero error checkng here
struct ParserHelper
{
	explicit ParserHelper(const char* path)
	    : m_stream(path, std::ios::in)
	{}

	bool is_good() const { return m_stream.good(); }

	template<typename T>
	std::vector<T> read_delimited(char delim)
	{
		std::vector<T> ret;
		do
		{
			T tok;
			m_stream >> tok;
			ret.push_back(std::move(tok));
		} while (m_stream.get() == delim);
		return ret;
	}

	template<typename Fn>
	std::vector<typename std::result_of<Fn(std::ifstream&)>::type> read_until(char delim, Fn&& f)
	{
		std::vector<typename std::result_of<Fn(std::ifstream&)>::type> ret;
		do
		{
			auto tok = f(m_stream);
			ret.push_back(std::move(tok));
		} while (m_stream.peek() != delim && m_stream.good());
		m_stream.ignore(1);
		return ret;
	}

	template<typename T, size_t N>
	std::array<T, N> read_delimited_n()
	{
		std::array<T, N> ret;
		read_delimited_n_into<T, N>(ret.data());
		return ret;
	}

	template<typename T, size_t N>
	void read_delimited_n_into(T* elem)
	{
		for (size_t i = 0; i < N; ++i)
		{
			m_stream >> *(elem + i);
			m_stream.ignore(1);
		}
	}

	ParserHelper& skip_line()
	{
		m_stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		return *this;
	}

	ParserHelper& skip_n(size_t n)
	{
		m_stream.ignore(n);
		return *this;
	}

	std::ifstream m_stream;
};
