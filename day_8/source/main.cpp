#include <algorithm>
#include <array>
#include <iostream>
#include <string>

#include "common.h"

int main()
{
	std::vector<std::pair<std::array<std::string, 10>, std::array<std::string, 4>>> displays;

	ParserHelper p("input.txt");

	while (p.is_good())
	{
		auto input = p.read_delimited_n<std::string, 10>();
		p.skip_n(2);
		auto output = p.read_delimited_n<std::string, 4>();
		std::for_each(input.begin(), input.end(), [](std::string& elem) { std::sort(elem.begin(), elem.end()); });
		std::for_each(output.begin(), output.end(), [](std::string& elem) { std::sort(elem.begin(), elem.end()); });
		displays.emplace_back(std::move(input), std::move(output));
	}

	// problem 1
	{
		size_t easy_digits = 0;
		for (const auto& disp : displays)
		{
			easy_digits += std::count_if(disp.second.cbegin(), disp.second.cend(), [](const std::string& elem) {
				return elem.size() == 2 || elem.size() == 4 || elem.size() == 3 || elem.size() == 7;
			});
		}
		std::cout << easy_digits << "\n";
	}

	// problem 2
	{
		auto total = 0;
		for (const auto& disp : displays)
		{
			const auto& input = disp.first;
			const auto& output = disp.second;

			// several assumptions of "input" structure by problem definition...no attempt to sanity check
			// several assumptions of input correctness hence blindly deref-ing iterators

			const auto& number_1 =
			    *std::find_if(input.cbegin(), input.cend(), [](const std::string& elem) { return elem.size() == 2; });

			const auto& number_4 =
			    *std::find_if(input.cbegin(), input.cend(), [](const std::string& elem) { return elem.size() == 4; });

			const auto& number_7 =
			    *std::find_if(input.cbegin(), input.cend(), [](const std::string& elem) { return elem.size() == 3; });

			//not required
			//const auto& number_8 =
			//    *std::find_if(input.cbegin(), input.cend(), [](const std::string& elem) { return elem.size() == 7; });

			const auto& number_6 = *std::find_if(input.cbegin(), input.cend(), [&number_1](const std::string& elem) {
				std::string tmp;
				if (elem.size() == 6)
				{
					std::set_intersection(
					    elem.cbegin(), elem.cend(), number_1.cbegin(), number_1.cend(), std::back_inserter(tmp));
					return (tmp.size() == 1);
				}
				return false;
			});

			const auto& number_0 =
			    *std::find_if(input.cbegin(), input.cend(), [&number_4, &number_6](const std::string& elem) {
				    std::string tmp; //string instead of vector here since: max bound of 7 and SSO guarantees no allocations
				    if (elem.size() == 6 && elem != number_6)
				    {
					    std::set_difference(
					        elem.cbegin(), elem.cend(), number_4.cbegin(), number_4.cend(), std::back_inserter(tmp));
					    return tmp.size() == 3;
				    }
				    return false;
			    });

			const auto& number_9 =
			    *std::find_if(input.cbegin(), input.cend(), [&number_6, &number_0](const std::string& elem) {
				    return (elem.size() == 6 && elem != number_6 && elem != number_0);
			    });

			const auto& number_5 = *std::find_if(input.cbegin(), input.cend(), [&number_6](const std::string& elem) {
				std::string tmp;
				if (elem.size() == 5)
				{
					std::set_difference(number_6.cbegin(), number_6.cend(), elem.cbegin(), elem.cend(), std::back_inserter(tmp));
					return tmp.size() == 1;
				}
				return false;
			});

			const auto& number_3 = *std::find_if(input.cbegin(), input.cend(), [&number_5](const std::string& elem) {
				std::string tmp;
				if (elem.size() == 5 && elem != number_5)
				{
					std::set_difference(number_5.cbegin(), number_5.cend(), elem.cbegin(), elem.cend(), std::back_inserter(tmp));
					return tmp.size() == 1;
				}
				return false;
			});

			const auto& number_2 =
			    *std::find_if(input.cbegin(), input.cend(), [&number_5, &number_3](const std::string& elem) {
				    return (elem.size() == 5 && elem != number_3 && elem != number_5);
			    });

			auto digit_multiplier = 1000;
			auto output_numeric = 0;
			//assert(output.size() == 4);
			for (int i = 0; i < output.size(); ++i)
			{
				const auto& o = output[i];
				auto number = 8;
				//clauses are ordered by the amount of lit segments for optimising string searches
				if (o == number_1)
					number = 1;
				else if (o == number_7)
					number = 7;
				else if (o == number_4)
					number = 4;
				else if (o == number_2)
					number = 2;
				else if (o == number_3)
					number = 3;
				else if (o == number_5)
					number = 5;
				else if (o == number_6)
					number = 6;
				else if (o == number_9)
					number = 9;
				else if (o == number_0)
					number = 0;
				number *= digit_multiplier;
				output_numeric += number;
				digit_multiplier /= 10;
			}

			total += output_numeric;
		}
		std::cout << total << "\n";
	}

	return 0;
}
