#include <iostream>
#include <numeric>
#include <algorithm>

#include "common.h"

int flood_fill(std::vector<std::vector<int>>& hm, int x, int y)
{
	if (hm[x][y] == 9)
		return 0;
	hm[x][y] = 9;
	return 1 + flood_fill(hm, x + 1, y) + flood_fill(hm, x - 1, y) + flood_fill(hm, x, y + 1) + flood_fill(hm, x, y - 1);
};

int main()
{
	// pad outer bounds of heightmap with 9s to avoid bounds checks
	std::vector<std::vector<int>> heightmap;
	ParserHelper p("input.txt");
	while (p.is_good())
	{
		auto row = p.read_until('\n', [](std::ifstream& str) {
			char digit;
			str >> digit;
			int d = digit - '0';
			return d;
		});
		row.insert(row.begin(), 9);
		row.push_back(9);
		heightmap.push_back(std::move(row));
	}
	std::vector<int> row(heightmap[0].size(), 9);
	heightmap.insert(heightmap.begin(), row);
	heightmap.push_back(std::move(row));

	const auto row_len = heightmap[0].size() - 1;
	const auto col_len = heightmap.size() - 1;

	// problem 1
	{
		auto risk_level = 0;
		for (int i = 1; i < col_len; ++i)
		{
			for (int j = 1; j < row_len; ++j)
			{
				const auto& elem = heightmap[i][j];
				const auto is_low_point = (heightmap[i + 1][j] > elem) && (heightmap[i - 1][j] > elem)
				                          && (heightmap[i][j + 1] > elem) && (heightmap[i][j - 1] > elem);
				if (is_low_point)
					risk_level += elem + 1;
			}
		}
		std::cout << risk_level << "\n";
	}

	// problem 2
	{
		std::vector<int> basins;
		for (int i = 1; i < col_len; ++i)
		{
			for (int j = 1; j < row_len; ++j)
			{
				const auto& elem = heightmap[i][j];
				if (elem != 9)  // from definition, every non 9 is part of only 1 basin
					basins.push_back(flood_fill(heightmap, i, j));
			}
		}
		std::partial_sort(basins.begin(), basins.begin() + 3, basins.end(), std::greater<int>());
		int result = std::accumulate(basins.begin(), basins.begin() + 3, 1, std::multiplies<int>());
		std::cout << result << "\n";
	}
	return 0;
}
